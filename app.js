const title = document.querySelector('.title');
const fbIcon = document.querySelector('.fbIcon');
const gmailIcon = document.querySelector('.gmailIcon');
const linkedinIcon = document.querySelector('.linkedinIcon');

title.addEventListener('mouseover', (e) => {
 e.preventDefault();
});

fbIcon.addEventListener('click', (e) => {
 e.preventDefault();
 window.open('https://www.facebook.com/profile.php?id=100004005377436')
})

gmailIcon.addEventListener('click', (e) => {
 e.preventDefault();
 window.open('mailto:przemyslaw.woznica97@gmail.com');
})

linkedinIcon.addEventListener('click', (e) => {
 e.preventDefault();
 window.open('https://www.linkedin.com/in/przemys%C5%82aw-wo%C5%BAnica-1440101b6')
})

/********** moveIcons *************/

anime({
 targets: '.linkedinIcon, .fbIcon, .gmailIcon, .phoneIcon',
 translateX: 590,
 delay: anime.stagger(20, {easing: 'easeOutQuad'})
});

/***************** Gmail Modal **********************/

// Get the modal
var modalGmail = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
  modalGmail.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modalGmail.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
document.onclick = function(event) {
  if (event.target == modalGmail) {
    modalGmail.style.display = "none";
  }
}

/***************** phoneNumber Modal **********************/

// Get the modal
var modalPhone = document.getElementById("myModalPhone");

// Get the button that opens the modal
var btnPhone = document.getElementById("myBtnPhone");

// Get the <span> element that closes the modal
var spanTwo = document.getElementsByClassName("closeTwo")[0];

// When the user clicks on the button, open the modal
btnPhone.onclick = function() {
  modalPhone.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
spanTwo.onclick = function() {
  modalPhone.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modalPhone) {
    modalPhone.style.display = "none";
  }
}

/* popupFb */

var eventFB = document.getElementById('myBtnFb');

eventFB.onmouseover = function() {
  document.getElementById('popupFb').style.display = 'block';
}

eventFB.onmouseout = function() {
  document.getElementById('popupFb').style.display = 'none';
}  

/* popupGmail */

var eventGmail = document.getElementById('myBtn');

eventGmail.onmouseover = function() {
  document.getElementById('popupGmail').style.display = 'block';
}

eventGmail.onmouseout = function() {
  document.getElementById('popupGmail').style.display = 'none';
}  

/* popupPhone */

var eventPhone = document.getElementById('myBtnPhone');

eventPhone.onmouseover = function() {
  document.getElementById('popupPhone').style.display = 'block';
}

eventPhone.onmouseout = function() {
  document.getElementById('popupPhone').style.display = 'none';
}  

/* popupLinkedin */

var eventLinkedin = document.getElementById('myBtnLinkedin');

eventLinkedin.onmouseover = function() {
  document.getElementById('popupLinkedin').style.display = 'block';
}

eventLinkedin.onmouseout = function() {
  document.getElementById('popupLinkedin').style.display = 'none';
}  


/* .fbIcon, .gmailIcon, .phoneIcon */